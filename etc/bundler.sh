#! /bin/bash

projecthome=$(cd $(dirname $0)/../ && pwd)
pushd $projecthome
exec bundle install --path=./vendor/bundle --binstubs=./.bundle/stubs "$@"
popd
